const currentBalance = document.getElementById("bank-balance");
const currentSalary = document.getElementById("work-salary");
const activeLoan = document.getElementById("loan-active");
const select = document.getElementById("laptops");
const informationDiv = document.getElementById("info-div");
const pictureDiv = document.getElementById("picture-div");
const buyButton = document.getElementById("buy-btn");
const loanButton = document.getElementById("loan-btn");
const repayButton = document.getElementById("payback-btn");

//URLS TO API
const apiURL = "https://noroff-komputer-store-api.herokuapp.com/computers";
const imageURL = "https://noroff-komputer-store-api.herokuapp.com/";


//All banking variables used
let priceLaptop = 0;
let titleLaptop = "";
let salary = 0;
let balance = 200;
let currentLoan = 0;
let workSalary = 0;
let loanPay = 0;

//Function for earning money, each time you hit the work button you'll receive 100KR
function earnings() {
  salary = salary + 100;
  currentSalary.innerHTML = "Salary:" + " " + salary + " kr:-";
}

//Function for adding the work balance into your bank account. Each time you'll have a loan you can deposit
// it'll take 10% fee and pay off the loan.
function addBalance() {
  if (currentLoan > 0) {
    let loanFee = salary * 0.1;
    salary = salary - loanFee;
    if (currentLoan < loanFee) {
      salary = salary + (loanFee % currentLoan);
    } else {
      currentLoan = currentLoan - loanFee;
    }
  }
  balance = balance + salary;
  salary = 0;
  currentBalance.innerHTML = "Balance:" + " " + balance + "Kr:-";
  currentSalary.innerHTML = salary + " Kr:-";
  activeLoan.innerHTML = "Loan:" + " " + currentLoan + "kr:-";
}

// function for getting a loan. It also updates the balance with the borrowed money.
function getLoan() {
  const LoanMessage = prompt("How much do you want to Loan", 0);

  if (LoanMessage > balance * 2) {
    alert("You cant take such big loan");
    loanButton.hidden = false;
  } else if (LoanMessage !== null) {
    balance = balance + parseInt(LoanMessage);
    currentBalance.innerHTML = "Balance:" + " " + balance + " kr:-";
    currentLoan = LoanMessage;
    activeLoan.innerHTML = "Loan:" + " " + currentLoan + " kr:-";
    loanButton.hidden = true;
    repayButton.hidden = false;
  }
}

//Function that makes you able to pay back on loan.
function returnLoan() {
  if (salary >= currentLoan) {
    loanPay = salary - currentLoan;
    salary = loanPay;
    currentLoan = 0;
    loanButton.hidden = false;
    repayButton.hidden = true;
  } else if (salary < currentLoan) {
    loanPay = currentLoan - salary;
    currentLoan = loanPay;
    salary = 0;
  }

  currentSalary.innerHTML = salary + "kr:-";
  activeLoan.innerHTML = "Loan: " +  currentLoan + "kr:-";
}
 
//Function that fetches data that populate data inside the select.
async function ComputerResponse() {
  const response = await fetch(apiURL, {
    method: "GET",
  });

  if (!response.ok) {
    throw new Error(`HTTP error!, status: ${response.status}`);
  }
  const computer = await response.json();
  console.log(computer);
  for (let i = 0; i < computer.length; i++) {
    const selectOptions = document.createElement("option");
    selectOptions.appendChild(document.createTextNode(computer[i].title));
    select.appendChild(selectOptions);
  }
}

//Function that Fetches specific data and populate the dynamic created div with data..
async function GetLaptopData() {
  const response = await fetch(apiURL, {
    method: "GET",
  });

  if (!response.ok) {
    throw new Error(`HTTP error!, status: ${response.status}`);
  }
  const computer = await response.json();
  let index = document.getElementById("laptops").selectedIndex;

  informationDiv.innerHTML = "";
  pictureDiv.innerHTML = "";

  const selectComputer = document.createElement("h1");
  const selectSpecs = document.createElement("p");
  const selectDescription = document.createElement("h4");
  const picture = document.createElement("img");
  const buyComputer = document.createElement("button");

  const linkFormatter = computer[index].image;
  picture.src = `${imageURL}${linkFormatter}`;

  selectComputer.appendChild(
    document.createTextNode(computer[index].title + " ")
  );
  selectDescription.appendChild(
    document.createTextNode(computer[index].description + " ")
  );
  selectComputer.appendChild(
    document.createTextNode(computer[index].price + " " + "KR:-")
  );
  for (let i = 0; i < computer[index].specs.length; i++) {
    selectSpecs.appendChild(document.createTextNode(computer[index].specs[i]));
  }

  buyButton.hidden = false;
  informationDiv.appendChild(selectComputer);
  informationDiv.appendChild(selectDescription);
  informationDiv.appendChild(selectSpecs);
  pictureDiv.appendChild(picture);

  priceLaptop = computer[index].price;
  titleLaptop = computer[index].title;
}

//Simple buy function that reduces the balance. If you not have enough money you cant buy it.
function buyComputer() {
  if (balance >= priceLaptop) {
    balance = balance - priceLaptop;
    alert(`You bought this one, ${titleLaptop}`);
    currentBalance.innerHTML = balance + "kr :-";
  } else {
    alert(`you do not have enough money to buy ${titleLaptop}`);
  }
}

repayButton.hidden = true;
buyButton.hidden = true;

//Starts when you render the page
ComputerResponse();
